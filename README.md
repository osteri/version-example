# version-example

Simple example of using https://gitlab.com/osteri/version/ C++ git version 
compile-time library.

### Build
```
cmake -S . -B build (cd build && make -j)
```

### Run
```
./build/exe
```
outputs:
`1.0.1`