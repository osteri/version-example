#include <iostream>
#include <version.hpp>

int main() {
  std::cout << git::get_version() << '\n';
  return EXIT_SUCCESS;
}